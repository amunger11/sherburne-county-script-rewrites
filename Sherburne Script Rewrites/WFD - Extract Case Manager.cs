﻿namespace Sherburne_Script_Rewrites
{
    using Hyland.Unity;
    using System;
    using System.Data;

    /// <summary>
    /// Sets property bag information for CaseManagersList and CaseManagersLeft based on 
    /// the values pulled from the current document. Also, the OnBase User ID keyword 
    /// is updated using the CaseManagersList property value.
    /// </summary>
    class WFD___Extract_Case_Manager : Hyland.Unity.IWorkflowScript
    {
        private const Diagnostics.DiagnosticsLevel DiagLevel = Diagnostics.DiagnosticsLevel.Verbose;
        private const string ScriptName = "WFD - Extract Case Manager";

        #region IWorkflowScript
        /// <summary>
        /// Implementation of <see cref="IWorkflowScript.OnWorkflowScriptExecute" />.
        /// <seealso cref="IWorkflowScript" />
        /// </summary>
        /// <param name="app"><see cref="Application"/></param>
        /// <param name="args"><see cref="WorkflowEventArgs"/></param>
        public void OnWorkflowScriptExecute(Hyland.Unity.Application app, Hyland.Unity.WorkflowEventArgs args)
        {
            app.Diagnostics.Level = DiagLevel;
            app.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Info, string.Format("Start Script: {0}", ScriptName));
            // We're setting ScriptResult to true
            // This becomes false, only if an Exception is caught
            args.ScriptResult = true;

            try
            {
                // Get the current active document
                Document _currentDocument = args.Document;
                if (_currentDocument == null)
                    throw new ApplicationException("No active document was found!");

                var propBag = args.PropertyBag;

                object caseManagers = null;

                if (!propBag.TryGetValue("CaseManagerList", out caseManagers))
                {
                    throw new ApplicationException(string.Format("[{0}] does not exist in Property Bag", "CaseManagerList"));
                }

                object[] caseManagerList = caseManagers.ToString().Split(';');

                using (DocumentLock docLock = _currentDocument.LockDocument())
                {
                    KeywordModifier keyMod = _currentDocument.CreateKeywordModifier();

                    keyMod.AddKeyword("ONBASE USER ID", (int)caseManagerList[0]);

                    keyMod.ApplyChanges();
                }

                string tempManagers = string.Empty;

                if (caseManagerList.Length > 0)
                {
                    propBag.Set("CaseManagersLeft", "True");

                    for (int i = 0; i < caseManagerList.Length; i++)
                    {
                        if (i == 0)
                        {
                            tempManagers = caseManagerList[i].ToString();
                        }
                        else
                        {
                            tempManagers += ";" + caseManagerList[i].ToString();
                        }
                    }

                    propBag.Set("CaseManagersList", tempManagers);
                }
                else
                {
                    propBag.Set("CaseManagersLeft", "False");
                }

            }
            catch (Exception ex)
            {
                if ((app != null) && (app.Diagnostics != null))
                {
                    app.Diagnostics.Write(String.Format("{0} (Script: {1}) (User: {2}) (Document: {3})",
                        ex.GetType().Name,
                        System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name,
                        app.CurrentUser.Name,
                        args.Document.ID.ToString()));

                    app.Diagnostics.Write(ex);
                    app.Diagnostics.Write(ex.StackTrace);
                    args.ScriptResult = false;
                }
            }
            finally
            {
                app.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Info, string.Format("End Script: {0}", ScriptName));
                app.Disconnect();
            }
        }
        #endregion
    }
}
