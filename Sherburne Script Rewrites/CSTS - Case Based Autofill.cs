﻿namespace Sherburne_Script_Rewrites
{
    using Hyland.Unity;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;

    /// <summary>
    /// Takes tha primary keyword value from the autofill keyword set and passes it into a ReturnResult method.
    /// Connects to the database using procedure "qryGetOnBase_Keyword_Values_All_ByClientCase" and populates a 
    /// datatable.
    /// </summary>
    class CSTS___Case_Based_Autofill : Hyland.Unity.IExternalAutofillKeysetScript
    {

        #region IExternalAutofillKeysetScript
        public void OnExternalAutofillKeysetScriptExecute(Hyland.Unity.Application app, Hyland.Unity.ExternalAutofillKeysetEventArgs args)
        {
            try
            {
                app.Diagnostics.Write("Starting CSTS - Case Based Autofill Script...");

                KeywordTypeList autofillKeywordTypes = args.KeywordTypes; //all keyword types in autofill
                Keyword primary = args.PrimaryKeyword; //primary keyword of the autofill
                ExternalAutofillKeysetEventResults results = args.Results; //result object to pass back to the client


                //Data 'row' to return
                ExternalAutofillKeysetData data = results.CreateExternalAutofillKeysetData();
                data.SetKeyword(primary);

                //const string connectionStringName = "Driver={SQL Server};server=pipes;Uid=OB_CSTS;Pwd=Dbsh3rc0;Database=TESTCSTS";
                const string connectionStringName = "CSTS Test";

                const string query = "qryGetOnBase_Keyword_Values_All_ByClientCase";

                const string KEYTYPE_CSTS_CASE_NUMBER = "CSTS Case Number";
                const string KEYTYPE_CSTS_CLIENT_NUMBER = "CSTS Client Number";
                const string KEYTYPE_CSTS_LAST_NAME = "CSTS Last Name";
                const string KEYTYPE_CSTS_FIRST_NAME = "CSTS First Name";
                const string KEYTYPE_CSTS_MIDDLE_NAME = "CSTS Middle Name";
                const string KEYTYPE_CSTS_SUFFIX = "CSTS Suffix";
                const string KEYTYPE_CSTS_DOB = "CSTS DOB";
                const string KEYTYPE_CSTS_SSN = "CSTS SSN";
                const string KEYTYPE_CSTS_ASSIGNED_AGENT = "CSTS Assigned Agent";
                const string KEYTYPE_CSTS_PENDING_REVIEW_AGENT = "CSTS Pending Review Agent";


                using (SqlConnection conn =  (SqlConnection)app.Configuration.GetConnection(connectionStringName))
                {
                    SqlCommand cmd = new SqlCommand();
                    SqlDataAdapter adapter = new SqlDataAdapter(query, conn);
                    DataTable dt = new DataTable();

                    cmd.Parameters.Add(new SqlParameter("@prm_Client_Number", ""));
                    cmd.Parameters.Add(new SqlParameter("@prm_Case_Number", primary.Value));
                    cmd.CommandType = CommandType.StoredProcedure;
                    adapter.SelectCommand = cmd;

                    conn.Open();
                    
                    DataSet dataSet = new DataSet();

                    adapter.Fill(dataSet);

                    dt = dataSet.Tables[0];

                    int counter = 0;
                    Keyword key = null;

                    app.Diagnostics.Write(string.Format("Reader contains {0} rows", dt.Rows.Count));

                    foreach (DataRow row in dt.Rows)
                    {
                        foreach (DataColumn col in dt.Columns)
                        {
                            if (col.ColumnName == "Case_Number")
                            {
                                KeywordType caseNumKT = autofillKeywordTypes.Find(KEYTYPE_CSTS_CASE_NUMBER);
                                if (caseNumKT == null)
                                {
                                    throw new Exception("Could not find keyword type " + KEYTYPE_CSTS_CASE_NUMBER + " in the keyword type list for this autofill.");
                                }
                                key = caseNumKT.CreateKeyword(row[col].ToString());
                                data.SetKeyword(key);
                            }

                            if (col.ColumnName == "Client_Number")
                            {
                                KeywordType clientNumKT = autofillKeywordTypes.Find(KEYTYPE_CSTS_CLIENT_NUMBER);
                                if (clientNumKT == null)
                                {
                                    throw new Exception("Could not find keyword type " + KEYTYPE_CSTS_CLIENT_NUMBER + " in the keyword type list for this autofill.");
                                }
                                key = clientNumKT.CreateKeyword(row[col].ToString());
                                data.SetKeyword(key);
                            }

                            if (col.ColumnName == "Name_Last")
                            {
                                KeywordType lastNameKT = autofillKeywordTypes.Find(KEYTYPE_CSTS_LAST_NAME);
                                if (lastNameKT == null)
                                {
                                    throw new Exception("Could not find keyword type " + KEYTYPE_CSTS_LAST_NAME + " in the keyword type list for this autofill.");
                                }
                                key = lastNameKT.CreateKeyword(row[col].ToString());
                                data.SetKeyword(key);
                            }

                            if (col.ColumnName == "Name_First")
                            {
                                KeywordType firstNameKT = autofillKeywordTypes.Find(KEYTYPE_CSTS_FIRST_NAME);
                                if (firstNameKT == null)
                                {
                                    throw new Exception("Could not find keyword type " + KEYTYPE_CSTS_FIRST_NAME + " in the keyword type list for this autofill.");
                                }
                                key = firstNameKT.CreateKeyword(row[col].ToString());
                                data.SetKeyword(key);
                            }

                            if (col.ColumnName == "Name_Middle")
                            {
                                KeywordType middleNameKT = autofillKeywordTypes.Find(KEYTYPE_CSTS_MIDDLE_NAME);
                                if (middleNameKT == null)
                                {
                                    throw new Exception("Could not find keyword type " + KEYTYPE_CSTS_MIDDLE_NAME + " in the keyword type list for this autofill.");
                                }
                                key = middleNameKT.CreateKeyword(row[col].ToString());
                                data.SetKeyword(key);
                            }

                            if (col.ColumnName == "Name_Suffix")
                            {
                                KeywordType suffixKT = autofillKeywordTypes.Find(KEYTYPE_CSTS_SUFFIX);
                                if (suffixKT == null)
                                {
                                    throw new Exception("Could not find keyword type " + KEYTYPE_CSTS_SUFFIX + " in the keyword type list for this autofill.");
                                }
                                key = suffixKT.CreateKeyword(row[col].ToString());
                                data.SetKeyword(key);
                            }

                            if (col.ColumnName == "Date_Of_Birth" && row[col] != null)
                            {
                                KeywordType dobKT = autofillKeywordTypes.Find(KEYTYPE_CSTS_DOB);
                                if (dobKT == null)
                                {
                                    throw new Exception("Could not find keyword type " + KEYTYPE_CSTS_DOB + " in the keyword type list for this autofill.");
                                }

								string dateString = row[col].ToString();

                                DateTime time = Convert.ToDateTime(dateString);
                                key = dobKT.CreateKeyword(time);
	                            data.SetKeyword(key);
                            }

                            if (col.ColumnName == "SSN")
                            {
                                KeywordType ssnKT = autofillKeywordTypes.Find(KEYTYPE_CSTS_SSN);
                                if (ssnKT == null)
                                {
                                    throw new Exception("Could not find keyword type " + KEYTYPE_CSTS_SSN + " in the keyword type list for this autofill.");
                                }
                                key = ssnKT.CreateKeyword(row[col].ToString());
                                data.SetKeyword(key);
                            }

                            if (col.ColumnName == "Assigned_Agent_Name")
                            {
                                KeywordType assignedAgentKT = autofillKeywordTypes.Find(KEYTYPE_CSTS_ASSIGNED_AGENT);
                                if (assignedAgentKT == null)
                                {
                                    throw new Exception("Could not find keyword type " + KEYTYPE_CSTS_ASSIGNED_AGENT + " in the keyword type list for this autofill.");
                                }
                                key = assignedAgentKT.CreateKeyword(row[col].ToString());
                                data.SetKeyword(key);
                            }

                            counter++;
                        }
                    }

                    dt = dataSet.Tables[3];
                    foreach (DataRow row in dt.Rows)
                    {
                        foreach (DataColumn col in dt.Columns)
                        {
                            KeywordType pendingReviewKT = autofillKeywordTypes.Find(KEYTYPE_CSTS_PENDING_REVIEW_AGENT);
                            if (pendingReviewKT == null)
                            {
                                throw new Exception("Could not find keyword type " + KEYTYPE_CSTS_PENDING_REVIEW_AGENT + " in the keyword type list for this autofill.");
                            }
                            key = pendingReviewKT.CreateKeyword(row[col].ToString());
                            data.SetKeyword(key);
                        }
                    }
                    

                    conn.Close();
                }

                //Add the data to the results
                results.AddExternalKeywordAutofillKeysetData(data);

            }
            catch (Exception ex)
                {
                    if ((app != null) && (app.Diagnostics != null))
                    {
                        app.Diagnostics.Write(String.Format("{0} (Script: {1}) (User: {2})",ex.GetType().Name,System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name,app.CurrentUser.Name));

                        app.Diagnostics.Write(ex);
                        app.Diagnostics.Write(ex.StackTrace);
                    }
                }
                finally
                {
                    app.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Info, string.Format("End Script: {0}", "CSTS - Case Based Autofill"));
                    app.Disconnect();
                }
        }
        #endregion
    }
}
