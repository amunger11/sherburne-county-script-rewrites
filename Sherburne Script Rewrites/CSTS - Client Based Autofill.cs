﻿namespace Sherburne_Script_Rewrites
{
    using Hyland.Unity;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;

    /// <summary>
    /// Takes tha primary keyword value from the autofill keyword set and passes it into a ReturnResult method.
    /// Connects to the database using procedure "qryGetOnBase_Keyword_Values_All_ByClientCase" and populates a 
    /// datatable.
    /// </summary>
    class CSTS___Client_Based_Autofill : Hyland.Unity.IExternalAutofillKeysetScript
    {
        private Hyland.Unity.Application _app;

        #region IExternalAutofillKeysetScript
        public void OnExternalAutofillKeysetScriptExecute(Hyland.Unity.Application app, Hyland.Unity.ExternalAutofillKeysetEventArgs args)
        {
            try
            {
                _app = app;
                app.Diagnostics.Write("Starting CSTS - Client Based Autofill Script...");

                KeywordTypeList autofillKeywordTypes = args.KeywordTypes; //all keyword types in autofill
                Keyword primary = args.PrimaryKeyword; //primary keyword of the autofill
                ExternalAutofillKeysetEventResults results = args.Results; //result object to pass back to the client

                //Note - each keyword must be added to the result set - even the primary keyword) 
                ReturnResult(results, primary, autofillKeywordTypes);

            }
            catch (Exception ex)
            {
                app.Diagnostics.Write(ex.Message);
                app.Diagnostics.Write(ex.StackTrace);
            }
        }

        /// <summary>
        /// Method that populates the results object. Will set the secondary values.
        /// </summary>
        /// <param name="results"></param>
        /// <param name="primaryKeyword"></param>
        /// <param name="keywordTypes"></param>
        private void ReturnResult(ExternalAutofillKeysetEventResults results, Keyword primaryKeyword, KeywordTypeList keywordTypes)
        {
            //Data 'row' to return
            ExternalAutofillKeysetData data = results.CreateExternalAutofillKeysetData();
            data.SetKeyword(primaryKeyword);

            //const string connectionStringName = "Driver={SQL Server};server=pipes;Uid=OB_CSTS;Pwd=Dbsh3rc0;Database=TESTCSTS";
            const string connectionStringName = "CSTS Test";

            const string query = "qryGetOnBase_Keyword_Values_All_ByClientCase";

            const string KEYTYPE_CSTS_CLIENT_NUMBER = "CSTS Client Number";
            const string KEYTYPE_CSTS_LAST_NAME = "CSTS Last Name";
            const string KEYTYPE_CSTS_FIRST_NAME = "CSTS First Name";
            const string KEYTYPE_CSTS_MIDDLE_NAME = "CSTS Middle Name";
            const string KEYTYPE_CSTS_SUFFIX = "CSTS Suffix";
            const string KEYTYPE_CSTS_DOB = "CSTS DOB";
            const string KEYTYPE_CSTS_SSN = "CSTS SSN";
            const string KEYTYPE_CSTS_ASSIGNED_AGENT = "CSTS Assigned Agent";
            const string KEYTYPE_CSTS_PENDING_REVIEW_AGENT = "CSTS Pending Review Agent";
            const string KEYTYPE_CSTS_BALANCE = "CSTS (Balance)";

            try
            {
                using (SqlConnection conn = (SqlConnection)_app.Configuration.GetConnection(connectionStringName))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.Parameters.Add(new SqlParameter("@prm_Client_Number", primaryKeyword.Value));
                    cmd.Parameters.Add(new SqlParameter("@prm_Case_Number", ""));
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();

                    DataSet dataSet = new DataSet();

                    adapter.Fill(dataSet);

                    dt = dataSet.Tables[0];

                    Keyword key = null;

                    foreach (DataRow row in dt.Rows)
                    {
                        foreach (DataColumn col in dt.Columns)
                        {
                            if (col.ColumnName == "Client_Number")
                            {
                                KeywordType clientNumKT = keywordTypes.Find(KEYTYPE_CSTS_CLIENT_NUMBER);
                                if (clientNumKT == null)
                                {
                                    throw new Exception("Could not find keyword type " + KEYTYPE_CSTS_CLIENT_NUMBER + " in the keyword type list for this autofill.");
                                }
                                key = clientNumKT.CreateKeyword(row[col].ToString());
                                data.SetKeyword(key);
                            }

                            if (col.ColumnName == "Name_Last")
                            {
                                KeywordType lastNameKT = keywordTypes.Find(KEYTYPE_CSTS_LAST_NAME);
                                if (lastNameKT == null)
                                {
                                    throw new Exception("Could not find keyword type " + KEYTYPE_CSTS_LAST_NAME + " in the keyword type list for this autofill.");
                                }
                                key = lastNameKT.CreateKeyword(row[col].ToString());
                                data.SetKeyword(key);
                            }

                            if (col.ColumnName == "Name_First")
                            {
                                KeywordType firstNameKT = keywordTypes.Find(KEYTYPE_CSTS_FIRST_NAME);
                                if (firstNameKT == null)
                                {
                                    throw new Exception("Could not find keyword type " + KEYTYPE_CSTS_FIRST_NAME + " in the keyword type list for this autofill.");
                                }
                                key = firstNameKT.CreateKeyword(row[col].ToString());
                                data.SetKeyword(key);
                            }

                            if (col.ColumnName == "Name_Middle")
                            {
                                KeywordType middleNameKT = keywordTypes.Find(KEYTYPE_CSTS_MIDDLE_NAME);
                                if (middleNameKT == null)
                                {
                                    throw new Exception("Could not find keyword type " + KEYTYPE_CSTS_MIDDLE_NAME + " in the keyword type list for this autofill.");
                                }
                                key = middleNameKT.CreateKeyword(row[col].ToString());
                                data.SetKeyword(key);
                            }

                            if (col.ColumnName == "Name_Suffix")
                            {
                                KeywordType suffixKT = keywordTypes.Find(KEYTYPE_CSTS_SUFFIX);
                                if (suffixKT == null)
                                {
                                    throw new Exception("Could not find keyword type " + KEYTYPE_CSTS_SUFFIX + " in the keyword type list for this autofill.");
                                }
                                key = suffixKT.CreateKeyword(row[col].ToString());
                                data.SetKeyword(key);
                            }

                            if (col.ColumnName == "Date_Of_Birth")
                            {
                                KeywordType dobKT = keywordTypes.Find(KEYTYPE_CSTS_DOB);
                                if (dobKT == null)
                                {
                                    throw new Exception("Could not find keyword type " + KEYTYPE_CSTS_DOB + " in the keyword type list for this autofill.");
                                }

                                string dateString = row[col].ToString();

                                DateTime time = Convert.ToDateTime(dateString);
                                key = dobKT.CreateKeyword(time);
                                data.SetKeyword(key);
                            }

                            if (col.ColumnName == "SSN")
                            {
                                KeywordType ssnKT = keywordTypes.Find(KEYTYPE_CSTS_SSN);
                                if (ssnKT == null)
                                {
                                    throw new Exception("Could not find keyword type " + KEYTYPE_CSTS_SSN + " in the keyword type list for this autofill.");
                                }
                                key = ssnKT.CreateKeyword(row[col].ToString());
                                data.SetKeyword(key);
                            }

                            if (col.ColumnName == "Assigned_Agent_Name")
                            {
                                KeywordType assignedAgentKT = keywordTypes.Find(KEYTYPE_CSTS_ASSIGNED_AGENT);
                                if (assignedAgentKT == null)
                                {
                                    throw new Exception("Could not find keyword type " + KEYTYPE_CSTS_ASSIGNED_AGENT + " in the keyword type list for this autofill.");
                                }
                                key = assignedAgentKT.CreateKeyword(row[col].ToString());
                                data.SetKeyword(key);
                            }
                        }
                    }

                    dt = dataSet.Tables[3];
                    foreach (DataRow row in dt.Rows)
                    {
                        foreach (DataColumn col in dt.Columns)
                        {
                            KeywordType pendingReviewKT = keywordTypes.Find(KEYTYPE_CSTS_PENDING_REVIEW_AGENT);
                            if (pendingReviewKT == null)
                            {
                                throw new Exception("Could not find keyword type " + KEYTYPE_CSTS_PENDING_REVIEW_AGENT + " in the keyword type list for this autofill.");
                            }
                            key = pendingReviewKT.CreateKeyword(row[col].ToString());
                            data.SetKeyword(key);
                        }
                    }

                    dt = dataSet.Tables[1];
                    foreach (DataRow row in dt.Rows)
                    {
                        foreach (DataColumn col in dt.Columns)
                        {
                            KeywordType balanceKT = keywordTypes.Find(KEYTYPE_CSTS_BALANCE);
                            if (balanceKT == null)
                            {
                                throw new Exception("Could not find keyword type " + KEYTYPE_CSTS_BALANCE + " in the keyword type list for this autofill.");
                            }
                            key = balanceKT.CreateKeyword(row[col].ToString());
                            data.SetKeyword(key);
                        }
                    }

                    conn.Close();
                }

                //Add the data to the results
                results.AddExternalKeywordAutofillKeysetData(data);

            }
            catch (Exception ex)
            {
                if ((_app != null) && (_app.Diagnostics != null))
                {
                    _app.Diagnostics.Write(String.Format("{0} (Script: {1}) (User: {2}) (Document: {3})",
                        ex.GetType().Name,
                        System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name,
                        _app.CurrentUser.Name));

                    _app.Diagnostics.Write(ex);
                    _app.Diagnostics.Write(ex.StackTrace);
                }
            }
            finally
            {
                _app.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Info, string.Format("End Script: {0}", "CSTS - Client Based Autofill"));
                _app.Disconnect();
            }
        }
        #endregion
    }
}
