﻿namespace Sherburne_Script_Rewrites
{
    using Hyland.Unity;
    using System;
    using System.Data;

    /// <summary>
    /// Sets CaseManagerList property bag to the concatenated CS OnBase User ID values and sets 
    /// the DOCTYPE keyword to the DocType current property bag value. And also clears the 
    /// AllRecipients property by setting it to an empty string.
    /// </summary>
    class WF___CS_WFD___Prep_Distribution : Hyland.Unity.IWorkflowScript
    {
        private const Diagnostics.DiagnosticsLevel DiagLevel = Diagnostics.DiagnosticsLevel.Verbose;
        private const string ScriptName = "WF - CS WFD - Prep Distribution";

        #region IWorkflowScript
        /// <summary>
        /// Implementation of <see cref="IWorkflowScript.OnWorkflowScriptExecute" />.
        /// <seealso cref="IWorkflowScript" />
        /// </summary>
        /// <param name="app"><see cref="Application"/></param>
        /// <param name="args"><see cref="WorkflowEventArgs"/></param>
        public void OnWorkflowScriptExecute(Hyland.Unity.Application app, Hyland.Unity.WorkflowEventArgs args)
        {
            app.Diagnostics.Level = DiagLevel;
            app.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Info, string.Format("Start Script: {0}", ScriptName));
            // We're setting ScriptResult to true
            // This becomes false, only if an Exception is caught
            args.ScriptResult = true;

            try
            {
                // Get the current active document
                Document _currentDocument = args.Document;
                if (_currentDocument == null)
                    throw new ApplicationException("No active document was found!");

                var propBag = args.PropertyBag;

                object docType = null;

                if (!propBag.TryGetValue("DocType", out docType))
                {
                    throw new ApplicationException(string.Format("[{0}] does not exist in Property Bag", "DocType"));
                }

                using(DocumentLock docLock = new DocumentLock())
                {
                    KeywordModifier keyMod = _currentDocument.CreateKeywordModifier();

                    keyMod.AddKeyword("DOCTYPE", docType.ToString());

                    keyMod.ApplyChanges();
                }

                string caseManagerList = string.Empty;

                foreach (var keyRec in _currentDocument.KeywordRecords)
                {
                    foreach (var key in keyRec.Keywords)
                    {
                        if (key.ToString().ToUpper() == "CS ONBASE USER ID")
                        {
                            if (caseManagerList == "")
                            {
                                caseManagerList = key.Value.ToString();
                            }
                            else
                            {
                                caseManagerList += ";" + key.Value.ToString();
                            }
                        }
                    }
                }

                propBag.Set("CaseManagerList", caseManagerList);
                propBag.Set("AllRecipients", string.Empty);

            }
            catch (Exception ex)
            {
                if ((app != null) && (app.Diagnostics != null))
                {
                    app.Diagnostics.Write(String.Format("{0} (Script: {1}) (User: {2}) (Document: {3})",
                        ex.GetType().Name,
                        System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name,
                        app.CurrentUser.Name,
                        args.Document.ID.ToString()));

                    app.Diagnostics.Write(ex);
                    app.Diagnostics.Write(ex.StackTrace);
                    args.ScriptResult = false;
                }
            }
        }
        #endregion
    }
}
