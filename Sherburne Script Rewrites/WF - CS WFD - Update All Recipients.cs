﻿namespace Sherburne_Script_Rewrites
{
    using Hyland.Unity;
    using System;
    using System.Data;

    /// <summary>
    /// Sets AllRecipients property value to the current document case manager keyword 
    /// value or a concatenation of the current AllRecipients value and case manager value.
    /// </summary>
    class WF___CS_WFD___Update_All_Recipients : Hyland.Unity.IWorkflowScript
    {
        private const Diagnostics.DiagnosticsLevel DiagLevel = Diagnostics.DiagnosticsLevel.Verbose;
        private const string ScriptName = "WF - CS WFD - Update All Recipients";

        #region IWorkflowScript
        /// <summary>
        /// Implementation of <see cref="IWorkflowScript.OnWorkflowScriptExecute" />.
        /// <seealso cref="IWorkflowScript" />
        /// </summary>
        /// <param name="app"><see cref="Application"/></param>
        /// <param name="args"><see cref="WorkflowEventArgs"/></param>
        public void OnWorkflowScriptExecute(Hyland.Unity.Application app, Hyland.Unity.WorkflowEventArgs args)
        {
            app.Diagnostics.Level = DiagLevel;
            app.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Info, string.Format("Start Script: {0}", ScriptName));
            // We're setting ScriptResult to true
            // This becomes false, only if an Exception is caught
            args.ScriptResult = true;

            try
            {
                // Get the current active document
                Document _currentDocument = args.Document;
                if (_currentDocument == null)
                    throw new ApplicationException("No active document was found!");

                string caseManager = string.Empty;

                foreach (var keyRec in _currentDocument.KeywordRecords)
                {
                    foreach (var key in keyRec.Keywords)
                    {
                        if (key.ToString().ToUpper() == "CS ONBASE USER ID")
                        {
                            caseManager = key.Value.ToString();
                        }
                    }
                }

                var propBag = args.PropertyBag;

                object allRecipients = null;

                if (!propBag.TryGetValue("AllRecipients", out allRecipients))
                {
                    propBag.Set("AllRecipients", caseManager);
                }
                else
                {
                    propBag.Set("AllRecipients", allRecipients + ", " + caseManager);
                }

            }
            catch (Exception ex)
            {
                if ((app != null) && (app.Diagnostics != null))
                {
                    app.Diagnostics.Write(String.Format("{0} (Script: {1}) (User: {2}) (Document: {3})",
                        ex.GetType().Name,
                        System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name,
                        app.CurrentUser.Name,
                        args.Document.ID.ToString()));

                    app.Diagnostics.Write(ex);
                    app.Diagnostics.Write(ex.StackTrace);
                    args.ScriptResult = false;
                }
            }
            finally
            {
                app.Diagnostics.WriteIf(Diagnostics.DiagnosticsLevel.Info, string.Format("End Script: {0}", ScriptName));
                app.Disconnect();
            }
        }
        #endregion
    }
}
